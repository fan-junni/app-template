import App from './App';
import * as Pinia from "pinia";
import { createUnistorage } from "pinia-plugin-unistorage";
import { createSSRApp } from 'vue';
import uviewPlus from '@/uni_modules/uview-plus';
import { initRequest } from '@/common/request';

export function createApp() {
	const app = createSSRApp(App);
	
	// 引入请求封装
	initRequest(app);
	
	const store = Pinia.createPinia();

	// 关键代码 👇
	store.use(createUnistorage());

	app.use(store).use(uviewPlus);
	
	return {
		app,
		Pinia, // 此处必须将 Pinia 返回
	}
}
