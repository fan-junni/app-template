import { defineStore } from "pinia";

export const useUserStore = defineStore("user", {
    state() {
        return {
            uname: "",
			upwd: "",
			token: "",
			menus: [],
			uinfo: {},
			upstatus: 0,//本次是否更新提示
        };
    },
    getters: {},
    actions: {
        setUname(uname) {
            this.uname = uname;
        },
        setUpwd(upwd) {
            this.upwd = upwd;
        },
		setToken(token) {
		    this.token = token;
		},
		setMenus(menus) {
			this.menus = menus;
		},
		setUinfo(uinfo) {
			this.uinfo = uinfo;
		},
		setUpstatus(upstatus) {
			this.upstatus = upstatus;
		},
		loginOut() {
            //this.uname = "";
			this.pword = "";
            this.token = "";
			this.upstatus = 0;
        },
    },
    unistorage: true, // 开启后对 state 的数据读写都将持久化
});

// 通过$subscribe实现持久化,
setTimeout(() => {
	const instance = useUserStore();
	instance.$subscribe((mutation, state) => {
		uni.setStorageSync(instance.$id, JSON.stringify(state))
	});
	const val = uni.getStorageSync(instance.$id);
	if (val) {
		instance.$state = JSON.parse(val);
	}
}, 2000)
