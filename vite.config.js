import { defineConfig, loadEnv  } from "vite";
import uni from "@dcloudio/vite-plugin-uni";

// https://vitejs.dev/config/
export default defineConfig(({mode})=>{
	const config = loadEnv(mode,__dirname)
	const { VITE_BASE_URL, VITE_BASE_XMMC } = config
	const target = VITE_BASE_URL + '/' + VITE_BASE_XMMC + '/';
	console.log('target======',target)
	return {
		plugins: [
		    uni(),
		],
		server: {
		    proxy: {
		        "/dev-api": {
		            target: target,
		            changeOrigin: true,
		            rewrite: (path) => path.replace(/^\/dev-api/, "/"),
		        },
		    },
		},
	}
    
});
