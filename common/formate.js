import dayjs from "dayjs";
import rTime from "dayjs/plugin/relativeTime";

// 全局使用中文
dayjs.locale("zh-cn");

// 对时间进行格式化
export function formatNTime(date = new Date(), type = "YYYY") {
    return dayjs(date).format(type);
}
export function formatNYTime(date = new Date(), type = "YYYY-MM") {
    return dayjs(date).format(type);
}
export function formatNYRTime(date = new Date(), type = "YYYY-MM-DD") {
    return dayjs(date).format(type);
}
export function formatSFMTime(date = new Date(), type = "HH:mm:ss") {
    return dayjs(date).format(type);
}
// 返回接口数据字段为空
export function formatEmpty(str){
	return str == null ? '/' : str;
}

export function formatNum(n){
	return n < 10 ? '0'+ n : n;
}

export function timeFormater(len){
	switch(true){
		case len==0 || len ==null:
			return '待处理';
			break;   
		case len>=86400:
			return '耗时'+Math.ceil(len/86400)+'天';
			break;    
		case len >=3600:
			return '耗时'+Math.ceil(len/3600)+'小时';
			break; 
		case len >=60 :
			return '耗时'+Math.ceil(len/60)+'分钟';
			break; 
		default:
			return '耗时'+Math.ceil(len)+'秒';   
			break;             
	}
}

export default {
    formatNYRTime,
	formatEmpty,
	formatNum
};
