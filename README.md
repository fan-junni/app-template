# app-template

#### 介绍
app-template是uni-app多端兼容移动端项目初始框架，即开即用。已经集成Pinia数据存储和用户登录等网络请求功能。

#### 软件架构
基于 vue3 + Vite + uni-app + uview-plus框架


#### 安装教程

1.  git clone https://gitee.com/fan-junni/app-template.git
2.  cd app-template
3.  pnpm install
4.  Hbuilder -> 文件 -> 打开目录，运行 -> 运行到浏览器 

