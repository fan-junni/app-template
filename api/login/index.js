
import { appName } from '@/common/config';

//公共api
const url = {
	login: `/${appName}/appLogin`,
	logout: `/${appName}/logout`
}

export const postLogin = (params, config = {}) => uni.$u.http.post(url.login, params, config);
export const postLogout = (params, config = {}) => uni.$u.http.post(url.logout, params, config);
